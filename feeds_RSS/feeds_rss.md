# Por que feeds RSS são fantásticos

---

Eu gosto MUITO de feeds RSS. Gosto tanto que quis fazer meu primeiro post aqui sobre eles.  

![Logo RSS](https://codeberg.org/pinoinha/Rasemala/raw/branch/main/RSS_como/rss_logo.png)

# O que é um feed RSS?  

A sigla não é importante, mas atualmente¹ significa **Really Simple Syndication**. Basicamente é um tipo de arquivo que permite distribuição de informação em larga escala, que pode ser indexado por programas feitos para isso (leitores RSS). Assim, você pode acompanhar sites, blogs, notícias, podcasts, etc. usando um feed RSS, criando newsletters das coisas que quiser ver. Se quiser os detalhes técnicos, pode ver na [Wikipedia](https://pt.wikipedia.org/wiki/RSS).  

# Por que RSS?  

Eu expressei alguns usos que faço do RSS acima, mas talvez você esteja se perguntando: "por que você usaria isso em primeiro lugar?". E eu acho uma dúvida totalmente válida, até porque já a tive. Mas aviso: uma vez que entendi o que os feeds RSS podiam fazer, me apaixonei completamente.

## Comodidade

Veja a seguinte captura de tela:

![Newsboat](https://codeberg.org/pinoinha/Rasemala/raw/branch/main/feeds_RSS/newsboat.png)
<p style="text-align: center;">*Imagem 1:* terminal com o programa Newsboat aberto</p>

Na mesma página encontram-se: subreddits de meu interesse, notícias acerca de distribuições GNU/Linux que eu gosto, canais do YouTube que eu sigo e um montão de blogs que eu gosto. Tudo isso num único menu, usando um único programa, diretamente do meu terminal.  

E, uma vez nessa janela inicial, eu posso navegar entre os feeds e ver os posts deles, lendo quaisquer um que eu quiser! Mas, além disso, posso pesquisar por palavras-chave, filtrar conteúdo que eu não queira ver, ordenar e reordenar... todas coisas que, se eu fosse fazer num site, precisaria chamar 10 APIS diferentes, lidar com a interface, depender da minha conexão com a internet... mas com um feed RSS eu não preciso! Tudo em texto pra que eu possa ler como quiser e processar como quiser. Se estiver em markdown eu abro num processador adequado, se estiver em html eu abro num navegador, tudo sem estresse e super simples.

### YouTube e Podcasts

Decidi fazer uma seção separada só pra falar sobre isso: feeds de RSS que envolvem **mídia**. Sabe isso que eu falei das postagens virem em formato de texto? Isso é legal e tudo o mais, mas e pros podcasts e canais do youtube? Eu faço o que?  

E a magia do RSS está aí: se configurado certinho, ele baixa os arquivos envolvidos na postagem! Ou seja, além do texto do podcast (por exemplo, a descrição do episódio), ele já puxa o áudio em si. E aí como processar e executar esse arquivo de áudio fica a seu critério; eu passo tudo pelo `mpv`. 

Ou seja, dessa maneira eu posso me inscrever nos canais de YouTube que eu quiser e assistir os vídeos deles sem jamais abrir o navegador! Tá tudo sempre ali pra mim, rodando localmente no meu player de vídeo, sem precisar de navegador ou clientes externos.  

Pra podcasts a mesma coisa: eu mando o arquivo de áudio diretamente pro player e ouço sem mais problemas. Bem legal, não?  

## Simplicidade

Como o RSS só retira o conteúdo "bruto" dos sites, eu consigo consumir com mais foco. Não tem um banner da página, um milhão de imagens espalhadas, pop-up com política de cookies ou outros empecilhos da Web moderna. Só conteúdo puro, que você processa e consome como quiser. Inclusive esse ponto é um dos motivos pelos quais eu gosto muito de navegadores baseados em texto, que eventualmente pretendo falar sobre também.  

## Ideologia

Claro que eu não deixaria de falar um pouco sobre ideologia. O que eu quero dizer com isso? Pois bem, leia meu parágrafo anterior novamente. O fato de eu não querer aceitar política de cookies de todo o site em que entro, não querer acessar sites como o YouTube diretamente, ouvir meus podcasts sem depender de alguma plataforma proprietária de streaming (embora eu faça às vezes, por comodidade) gira em torno do fato de que eu não estou satisfeito nem confortável em aceitar os moldes atuais da Web. Não quero ter que dar meus cookies e nem acesso à minha localização pra cada site que eu entro. Ou melhor, pra nenhum deles, a menos que necessário.  

Um dos motivos por eu jamais querer seguir a carreira de web designer (se é que esse nome ainda existe, porque hoje em dia é back-end, front-end, UX, UI, e outras coisas marketeiras) é porque eu desprezo os sites modernos e suas implementações. Sim, eles são mais bonitos e responsivos, e eu acho isso importante inclusive por uma questão de acessibilidade. Mas por trás de todo esse design há centenas de MB (quando não GB!) de RAM sendo consumidos pelo SEU computador pra que o Facebook saiba que você está interessado em comprar um computador novo. Pra que o Google te dê anúncios personalizados pra algum produto de caráter pessoal, que você não queria que alguém soubesse. 

E mais: um site orientado a mouse é sim mais acessível, mas quando você usa 5 frameworks diferentes com 10 bibliotecas cada um você só faz com que o Seu Armando, dono da quitanda, leve 5 minutos pra carregar sua página por completo. Porque ele não tem um computador moderno, e muito menos um smartphone_ de última geração. Quandoa  página carregar vai ser acessível sim, o problema é chegar lá.  

Mas perdi um pouco a linha. Esse texto é sobre feeds RSS.  

# Conclusão

Eu amo feeds RSS e acho que deixei isso bem claro. Não há nada mais belo que a integração, comodidade e simplicidade que feeds RSS trazem. Eu sinceramente acredito que usar RSS é uma das maneiras mais elegantes e práticas de se consumir conteúdo da Internet, e espero que esse texto tenha despertado seu interesse :)  

Acabei não citando ao longo do texto, mas meu agregador de feeds é o [Newsboat](https://newsboat.org/). Mas se quiser outra opções, sugiro dar uma olha na [Arch Wiki](https://wiki.archlinux.org/title/Web_feed) (infelizmente em inglês).  

Talvez eu faça um outro texto mostrando como conseguir os feeds RSS das fontes que uso, como Reddit, YouTube, etc., mas só o tempo dirá.  

---
¹Digo atualmente  pois o significado mudou ao longo da evolução do formato, mais informações na página da Wikipedia indexada.
