MD_DIR := markdown
MD_FILES := $(filter-out $(MD_DIR)/index.md $(MD_DIR)/contato.md, $(wildcard $(MD_DIR)/*.md))

HTML_DIR := blog
HTML_FILES := $(patsubst $(MD_DIR)/%.md, $(HTML_DIR)/%.html, $(MD_FILES))

PANDOC := pandoc

PANDOC_OPTS := \
	-s \
	--template templates/padrao.html \
	--css /styles/style.css 

PANDOC_METADATA = \
	--metadata author="pinoinha" \
	--metadata date="$(shell date -I)"

PANDOC_COMMAND = $(PANDOC) $(PANDOC_OPTS) $(PANDOC_METADATA)

.PHONY: all
all: posts info

.PHONY: clean
clean:
	rm -rf blog
	rm index.html contato.html

.PHONY: posts
posts: $(HTML_FILES)

$(HTML_DIR)/%.html: $(MD_DIR)/%.md
	mkdir -p $(HTML_DIR)
	$(PANDOC_COMMAND) -i $< -o $@

.PHONY: info
info: index.html contato.html

index.html: $(MD_DIR)/index.md
	$(PANDOC_COMMAND) -i $< -o $@ 

contato.html: $(MD_DIR)/contato.md
	$(PANDOC_COMMAND) -i $< -o $@