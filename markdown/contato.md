---
title: contato
description: "meus contatos pra batermos um papo"
---

Caso você queira me contatar de alguma maneira, seguem meus principais meios de comunicação:

* minha [conta no mastodon](https://mastodon.com.br/@Pinoinha)
* email: pinoinha arroba disroot.org
* xmpp (não acesso faz algum tempo, mas vai que): pinoinha arroba disroot.org
* IRC: pinoinha no libera.chat, tilde.town e outros
