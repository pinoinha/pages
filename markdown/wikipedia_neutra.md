---
title: "A Wikipédia é uma plataforma neutra?"
description: "ensaio sobre a neutralidade ilusória da Wikipedia sob a perspectiva de seus artigos sobre Stalin"
---

"mas a Wikipedia não é neutra?"

1) pra afirmar que Pachukanis foi perseguido, "corrigido" e executado por Stálin são apresentadas *zero* fontes na página; no entanto, essa passagem está no artigo principal sobre a vida de Pachukanis¹

na Wikipedia em inglês há uma página só sobre a questão da fome na Ucrânia ter sido ou não um genocídio. apesar disso