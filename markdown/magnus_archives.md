---
title: "The Magnus Archives: uma história sobre trauma"
description: "reflexões sobre Magnus Archives, seus temas, mensagens e, inevitavelmente, seu fim"
---

<div class="AVISO" style="background-color: #e60053;">

**Aviso**: o texto a seguir cita assuntos sensíveis, tais quais: trauma,
relações tóxicas, violência física e psicológica e diversos tipos de
fobias. Além disso, spoilers pesados à frente.

</div>

## Parte 1: Sinopse
