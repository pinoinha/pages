---
title: pinoinha
description: pensamentos, reviews, nerdices e aleatoriedades
---

## O que é?  

Eu gosto de blogs e das pessoas que os escrevem, então quis fazer o meu próprio depois de tantos anos. É uma maneira de tentar escrever algo que eu gostaria de ler.  

## O que você deveria esperar deste blog?  

Pretendo postar sobre as coisas que eu gosto, tais quais, mas não somente:
  
* Linux
* *BSDs
* Programação
* Universidade pública
* Política (comunismo, especialmente)
* Música
* Demais interesses e pensamentos que me vierem à telha

## Como eu posso acompanhar esse blog?  

Use meu feed RSS! Você pode acessá-lo por [aqui](https://codeberg.org/pinoinha/pages.rss).

## Quem é você?

Minha persona virtual é só "pinoinha", mesmo.

Tenho 20 e poucos anos, curso Engenharia Elétrica (por enquanto), trabalho com software e sou comunista, mais especificamente marxista-leninista.

Sou uma pessoa não-branca e (provavelmente) não-hetero. Meus pronomes são ele/dele.

As pessoas dizem que eu sorrio bastante.

Se quiser, tenho um [perfil no Mastodon](http://mastodon.com.br/@Pinoinha), que basicamente é esse blog mas 24h/dia 7 dias/semana. 

## Código

Esse blog está hospedado usando o [Codeberg Pages](https://codeberg.page/), portanto tudo que eu subo está hospedado [nessa página](https://codeberg.org/pinoinha/pages) do Codeberg.

## Licenciamento

<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Licença Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />Este blog está licenciado com uma Licença <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Creative Commons Atribuição-NãoComercial 4.0 Internacional</a>.