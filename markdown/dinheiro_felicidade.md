---
title: "Dinheiro me traz felicidade?"
description: "reflexões sobre minha vida agora que trabalho remunerado"
---

Esse texto vai ser mais um fluxo de pensamento e reflexão que qualquer
outra coisa. Nada aqui tem a pretensão de seguir um fio lógico/racional
ou "solucionar" quaisuquer problemas que eu esteja tendo. É mais um
desabafo e uma tentativa de expressar em palavras o que eu venho
pensando.

## Meu primeiro emprego

No final de 2021 eu consegui meu primeiro emprego. Fui professor de
Python durante alguns meses, de dezembro de 2021 até março de 2022. Foi
um período interessante, e uma ótima experiência. As razões que me
levaram a sair da empresa são algumas e talvez mereçam sua própria
postagem.

Mas o foco aqui não é o emprego em si, mas sim uma consequência direta
de estar empregado: receber um salário.

Eu não consigo descrever a felicidade (ou seria êxtase?) que eu senti
quando abri minha conta e lá estavam singelos 1200 reais. Eu trabalhava
como MEI e estava em treinamento, então a quantia não era exorbitante.
Mas ainda assim, eu nunca tive tanto dinheiro na minha conta antes. E
mais: dinheiro que era **só meu**. Sem ser algo que meus pais me deram
ou algo do tipo. **Eu** ganhei aquilo. E eu podia fazer o que quisesse
com ele.

O primeiro salário acabou… rápido. Um jovem com dinheiro na conta e
muitos desejos de consumo não satisfeitos faz maravilhas em alguns dias.
Comprei presentes, comprei coisas pra mim, saí pra comer com minha
companheira por puro capricho. Só parei quando o dinheiro acabou.

Mas mal sabia eu o que estava por vir.

## O segundo salário

Ok, eu sabia que eventualmente o segundo salário viria. Mas não é o
segundo salário em si de que tô falando.

Quando eu recebi o PIX da empresa eu não pudia acreditar. Eu abri minha
conta pra conferir. Era real. Eu agora tinha mais de **dois mil reais**
na conta.

E se antes eu comprei até o que não podia… as coisas escalaram um pouco.

Eu gastei mais da metade do dinheiro em pouco mais de uma semana. Mais
presentes aqui e ali, mais saídas pra lugares um tanto mais caros do que
deveriam ser, mais compras. Eu lembro quando percebi o quanto tinha
gasto num período tão curto de tempo. Fiquei preocupado, mas não o
suficiente pra parar (afinal, ainda assim era MUITO dinheiro na minha
perpectiva).

O terceiro salário foi bem parecido, com a diferença de que eu já tinha
voltado às aulas presenciais e, portanto, tinha muitas mais maneiras de
gastar meu dinheiro no dia-a-dia. E é aí que mora o perigo.

## O fim da festa

Conforme comentei mais acima, eu saí do emprego em março, por uma série
de razões. O que signfica que, uma vez que aquele salário na conta se
foi, eu não tinha mais de onde tirar dinheiro. E aí coisas começaram a
acontecer.

## Como assim meu dinheiro acabou?!

…foi o que eu pensei quando, ao comprar um delicioso Trento torta de
limão na faculdade, percebi que minha conta estava igual a antes de
conseguir um emprego: vazia, fora alguns poucos reais. E aí olhei o
histórico de transações, incrédulo.

Pizzaria, presente, besteiras do dia-a-dia… nada extravagante. Ao menos
não individualmente. O problema é que eram **muitas** transações. E,
lendo cada uma, eu percebi que me lembrava de todas, ou seja: todo
aquele gasto de dinheiro foi deliberado, consciente.

E aí começou um processo muito difícil de aceitar que eu fui
extremamente consumista e irresponsável com meu próprio dinheiro. E se
eu precisasse pra alguma emergência? Esse e outros questionamentos
começaram a me pairar pela cabeça.

O próximo mês seguiu como antes: recebo uma mesada do meu pai, gasto
quase tudo pagando a psicóloga e necessidades básicas (alimentação,
transporte público) e o pouquinho que sobra uso pra mim.

## Eu era mais feliz com dinheiro?

E chegamos ao cerne da questão. Desde que eu deixei de ter um salário
estável, comecei a sentir tanto estresse com relação a dinheiro!
Acontece que agora qualquer saidinha pra comer que custe 20 reais por
pessoa (40 comigo e minha companheira) é uma facada imensa.

Eu gostava muito de ter dinheiro porque podia fazer esse tipo de coisa
com uma certa frequência, sem ter que me arrepender a cada mordida.
Disso eu tenho saudade. Também queria conseguir comprar as coisas que eu
quero: um fone novo, um Raspbery Pi, um domínio, um ThinkPad. E eu não
consigo fazer isso agora, e não vou conseguir por algum tempo.

Mas ao mesmo tempo, o ritmo em que eu gastei meus salários foi absurdo.
Definitivamente não pode se repetir para as próximas vezes,
especialmente considerando um futuro (não tão) distante em que eu
precise do dinheiro pra me manter sozinho. E não ter mais dinheiro me
impulsiona a tentar viver os dias com menos. Até agora tem funcionado…
mais ou menos.

Tenho muito pouco dinheiro na conta, mas em compensação tenho
desenvolvido hábitos pra gastar menos, como levar comida de casa pra
evitar ficar gastando na faculdade.

Então pra responder a pergunta desse tópico: eu definitivamente tinha
mais poder quanto tinha dinheiro. Podia fazer mais coisas, ajudar mais
pessoas. Isso eu espero recuperar.

Mas a sensação de gastar tudo tão rápido me deixava bem chateado. Me
fazia sentir mal, muito mal. Pelo menos não tenho mais ela agora…

Além disso, um detalhe é que tudo que eu planejava comprar conforme
comentei acima eu não consegui. Nem uma das coisas, apesar de ter o
dinheiro pra tal! Ou seja, atrasei minhas metas sem necessidade alguma.

## Meus próximos passos

Essa é fácil:

- conseguir um estágio
- comprar meu ThinkPad (+ o que der)
- aplicar as lições que aprendi pra enconomizar dinheiro
- lucro (será?)
